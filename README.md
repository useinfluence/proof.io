# Proof : UseInfluence

## Introduction

- Use influence allows website owners to increase website conversion using social proof using methods.
- Live activity notifications 
- Live visitor count
- Visitor identification
- Customer journey / path

## Providing configuration on the above methods as below

- Provide custom timing 
- Custom rules for sending notifications
- Optimisation
- Language



### Development 

Install strapi  and docker.

``
npm install strapi@alpha -g
``

Install Docker if you dont have already. Configure docker instances inside the `/docker` directory


```
cd docker
docker-compose up -d  
```

Once you are finished with above and everything seems alright time to fire 

``
strapi start
``

Then go at ``localhost:1337/admin`` and create your super user. You are good to go.

Please read  [strapi document](https://strapi.io/documentation/)

Thanks



```
Authors 
   
   Saransh Sharma
   
Founders
   
   Raman Parashar , Kirti Prakash 
 ```

